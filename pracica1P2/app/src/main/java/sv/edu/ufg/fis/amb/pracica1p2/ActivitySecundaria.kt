package sv.edu.ufg.fis.amb.pracica1p2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ActivitySecundaria : AppCompatActivity() {

    private lateinit var btnAtras : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secundaria)

        btnAtras = findViewById(R.id.btn_volver)

        btnAtras.setOnClickListener{
            finish()
        }

    }
}