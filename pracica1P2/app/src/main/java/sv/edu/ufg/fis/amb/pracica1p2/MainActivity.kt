package sv.edu.ufg.fis.amb.pracica1p2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {

    private lateinit var btnEjecutar : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEjecutar = findViewById(R.id.btn_disparador)

        btnEjecutar.setOnClickListener {
            var intent = Intent(this, ActivitySecundaria::class.java)
            startActivity(intent)
        }
    }
}